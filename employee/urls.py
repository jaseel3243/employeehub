from django.contrib import admin

from django.urls import path

from.import views

urlpatterns = [
    path('', views.home,name='home'),
    path('home', views.home,name='home'),
    path('addemployee', views.addemployee,name='addemployee'),
    path('viewemployee', views.viewemployee,name='viewemployee'),
    path('insert', views.insert,name='insert'),

]