from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import employee

# Create your views here.
def home(request):
    return render(request,'home.html')

def addemployee(request):

    return render(request,'addEmployee.html')

def viewemployee(request):
    context={'employees':employee.objects.all()}
    return render(request,'viewEmployees.html',context)        

def insert(request):
    try:
        name=request.GET['name']
        company=request.GET['company']
        salary=request.GET['salary']
        
        p=employee(name=name,company=company,salary=salary)
        p.save()
        
        return HttpResponse("<script>alert('success');window.location.href='viewemployee'</script>")
    except:
       
        return HttpResponse("<script>alert('sorry');window.location.href='addemployee'</script>")
    finally:
        print("MySQL connection is closed")    